#!/bin/bash
arch=$1
pair=$2
highreslang=$(echo $pair|sed -e 's/--/:/g'|cut -d: -f1)
lowreslang=$(echo $pair|sed -e 's/--/:/g'|cut -d: -f2)
python3 src/train.py \
    --dataset sigmorphon19task1 \
    --train sample/task1/$pair/$highreslang-train-high sample/task1/$pair/$lowreslang-train-low  \
    --dev sample/task1/$pair/$lowreslang-dev \
    --model model/sigmorphon19/task1/monotag-$arch/$pair --seed 0 \
    --embed_dim 10 --src_hs 5 --trg_hs 5 --dropout 0.2 \
    --src_layer 1 --trg_layer 1 --max_norm 5 \
    --arch $arch --estop 1e-8 --epochs 10 --bs 5 --mono
